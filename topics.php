<?php require('core/init.php'); ?>

<?php
$topic = new Topic;

$category = isset($_GET['category']) ? $_GET['category'] : null;

$user_id = isset($_GET['user']) ? $_GET['user'] : null;

$template = new Template('templates/topics.php');

if(isset($category)){
	$template->topics = $topic->getbycategory($category);
	$template->title = 'Posts in'.$topic->getcategory($category)->name;
}

if(isset($user_id)){
	$template->topics = $topic->getbyuser($user_id);
	//$template->title = 'Posts by'.$user->getUser($user_id)->username;
}


if(!isset($category) && !isset($user_id)){
	$template->topics = $topic->getAllTopics();
}


$template->totaltopics = $topic->gettotaltopics();
$template->totalcategories = $topic->gettotalcategories();

echo $template;

