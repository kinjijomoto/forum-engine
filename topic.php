<?php require('core/init.php'); ?>

<?php

$topic = new Topic;

$topic_id=$_GET['id'];

if (isset($_POST['do_reply'])) {
	$validate= new Validator;
		$data = array();
		$data['topic_id'] = $_GET['id'];
		$data['body'] = $_POST['body'];
		$data['user_id'] = getuser()['user_id'];

	$field_array = array('body');
		if ($validate->isrequired($field_array)) {
			if ($topic->reply($data)) {
					redirect('topic.php?id='.$topic_id,'reply posted', 'success');
				}else{
					redirect('topic.php?id='.$topic_id,'Smth wrong with youd data, try again', 'error');}
			}else{
					redirect('topic.php?id='.$topic_id,'Blank', 'error');}

	}


$template = new Template('templates/topic.php');

$template->topic = $topic->getTopic($topic_id);
$template->replies = $topic->getReplies($topic_id);
$template->title = $topic->getTopic($topic_id)->title;

echo $template;

