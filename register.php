<?php require('core/init.php'); ?>

<?php

$topic = new Topic;
$user = new User;
$validate = new validator;

if (isset($_POST['register'])) {
	$data = array();
	$data['name'] = $_POST['name'];
	$data['email'] = $_POST['email'];
	$data['username'] = $_POST['username'];
	$data['password'] = md5($_POST['password']);
	$data['password2'] = md5($_POST['password2']);
	$data['about'] = $_POST['about'];
	$data['last_activity'] = date("Y-m-d H:i:s");

	$field_array = array('name', 'email', 'username', 'password', 'password2');

	if ($validate->isrequired($field_array)) {
		if ($validate->isemail($data['email'])) {
			if($validate->passmatch($data['password'],$data['password2'])){
					if ($user->uploadavatar()) {
						$data['avatar'] = $_FILES['avatar']['name'];
					}else{
						$data['name'] = 'av.jpg' ;
					}

					if ($user->register($data)) {
						redirect('index.php', 'Successfully registration', 'success');
					}else{
						redirect('index.php', 'Oops smth went wrong', 'error');
					}
			}else{
				redirect('register.php','passwords didnt match', 'error');}
		}else{redirect('register.php','wrong email address', 'error');}
	}else{redirect('register.php','Please submit all fields', 'error');}}

$template = new Template('templates/register.php');
echo $template;

