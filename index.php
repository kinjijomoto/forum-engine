<?php require('core/init.php'); ?>

<?php

$topic = new Topic;
$user = new User;

$template = new Template('templates/frontpage.php');
$template->topics = $topic->getAllTopics();
$template->totaltopics = $topic->gettotaltopics();
$template->totalcategories = $topic->gettotalcategories();
$template->totalusers = $user->gettotalusers();
echo $template;

