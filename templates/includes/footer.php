          
           </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="sidebar">
            <div class="block">
              <h3>Login form</h3>
              <?php if(isloggedin()) : ?>
                <div class="userdata">
                  Welcome <?php echo getuser()['username']; ?>
                </div>
                <br>
                <form role="form" method="post" action="logout.php">
                  <input type="submit" name="do_logout" class="btn btn-default" value="logout">
                </form>
              <?php else : ?>
                <form role="form" method="post" action="login.php">
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" placeholder="Enter username">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input name="password" type="password" class="form-control" placeholder="enter password">
                </div>
                  <button name="do_login" type="submit" class="btn btn-primary">login</button> <a class="btn btn-default" href="register.php">Create account</a>
                </form>
              <?php endif; ?>
            </div>
            <div class="block">
            <h3>Categories</h3>
            <div class="list-group">
              <a href="topics.php" class="list-group-item <?php echo active(null); ?>">All topics <span class="badge pull-right"></span></a>
              <?php foreach(getcategories() as $category) : ?>
                <a href="topics.php?category=<?php echo $category->id; ?>" class="list-group-item <?php echo active($category->id); ?>"><?php echo $category->name; ?></a>
              <?php endforeach; ?>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div><!-- /.container -->


  </body>
</html>
