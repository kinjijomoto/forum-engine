<?php include('includes/header.php'); ?>

<form role="form" enctype="multipart/form-data" method="post" action="register.php">
  <div class="form-group">
    <label>Name*</label><input type="text" class="form-control" name="name" placeholder="Enter your name">
  </div>
  <div class="form-group">
    <label>Email</label><input type="email" class="form-control" name="email" placeholder="Enter your email">
  </div>
  <div class="form-group">
    <label>Username</label><input type="text" class="form-control" name="username" placeholder="create username">
  </div>
  <div class="form-group">
    <label>Password</label><input type="password" class="form-control" name="password" placeholder="Enter password">
  </div>
  <div class="form-group">
    <label>Check Password</label><input type="password" class="form-control" name="password2" placeholder="Enter same password">
  </div>
  <div class="form-group">
    <label>Upload avatar</label><input type="file" name="avatar"><p class="help-block"></p>
  </div>
  <div class="form-group">
    <label>About me</label><textarea id="about" rows="6" cols="80" class="form-control" name="about" placeholder="tell us about yourself"></textarea>
  </div>
  
  <input name="register" type ="submit" class="btn btn-default" value="Register">
</form>

<?php include('includes/footer.php'); ?>