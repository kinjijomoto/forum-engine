<?php include('includes/header.php'); ?>

  <form role="form" method="post">
    <ul id="topics">
      <li id="main-topic" class="topic topic">
        <div class="row">
          <div class="col-md-2">
            <div class="user-info">
              <img class="avatar pull-left" src="<?php echo BASE_URI; ?>img/avatars/<?php echo $topic->avatar; ?>">
              <ul>
                <li><strong><?php echo $topic->username; ?></strong></li>
                <li><?php echo Userpostcount($topic->user_id); ?> posts</li>
                <li><a href="<?php echo BASE_URI; ?>topics.php?user=<?php echo $topic->user_id; ?>">View topics</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="topic-content pull-right">
              <?php echo $topic->body; ?>
            </div>
          </div>
        </div>
      </li>
    <?php foreach($replies as $reply) : ?>  
      <li class="topic topic">
        <div class="row">
          <div class="col-md-2">
            <div class="user-info">
              <img class="avatar pull-left" src="<?php echo BASE_URI; ?>img/avatars/<?php echo $reply->avatar; ?>">
              <ul>
                <li><strong><?php echo $reply->username; ?></strong></li>
                <li><?php echo userpostcount($topic->user_id); ?> posts</li>
                <li><a href="<?php echo BASE_URI; ?>topics.php?user=<?php echo $reply->user_id; ?>">View topics</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-10">
            <div class="topic-content pull-right">
              <?php echo $reply->body; ?>
            </div>
          </div>
        </div>
      </li>
    <?php endforeach ; ?>

    </ul>
    <?php if(isloggedin()) : ?>
        <h3>Reply to topic</h3>
        <form role="form" method="post" action="topic.php?id=<?php echo urlFormat($topic->id); ?>">
          <div class="form-group">
            <textarea id="reply" rows="10" cols="80" class="form-control" name="body"></textarea>
              <script>CKEDITOR.replace('reply');</script>
          </div>
        <button name="do_reply" type="submit" class="btn btn-default">Reply</button>
        </form>
        <?php else : ?>
            <p></p>
        <?php endif; ?>
</form>

<?php include('includes/footer.php'); ?>